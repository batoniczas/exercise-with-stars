package online.tinystudyofallmatters.technicalexercise;

public class Exercise {
    //Zadanie techniczne
    //
    //Napisz program w javie który wypisze następujący wzór:
    //*
    //***
    //**
    //****
    //***
    //*****
    //****
    // ****** i tak dalej
    public static void main(String[] args) {
        String one = "*";
        String two = "***";

        for (int i = 0; i < 20; i++) {
            System.out.println(one);
            System.out.println(two);
            one +="*";
            two +="*";
        }
    }
}

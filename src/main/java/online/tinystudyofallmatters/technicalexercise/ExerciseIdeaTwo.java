package online.tinystudyofallmatters.technicalexercise;

public class ExerciseIdeaTwo {
    //Zadanie techniczne
    //
    //Napisz program w javie który wypisze następujący wzór:
    //*
    //***
    //**
    //****
    //***
    //*****
    //****
    // ****** i tak dalej
    public static void main(String[] args) {
        int i = 1;
        boolean add = true;
        while (i < 20) {
            printStar(i);
            if (add) {
                i += 2;
            } else {
                i--;
            }
            add = !add;
            System.out.println();
        }
    }
    public static void printStar(int i) {
        for (int j = 0; j < i; j++) {
            System.out.print("*");
        }
    }
}
